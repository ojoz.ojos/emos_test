<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
    body {
        margin: auto;
        max-width: 860px;
    }

    .container1 {
        padding: 10px;
        background-color: #71a4c9;
    }

    .container2 {
        padding: 10px;
    }

    .flex1 {
        display: flex;
        justify-content: space-between;
    }
</style>

<body>

<div>
    <div class="flex1 container1">
        <div>{{ $email }}</div>
        <div>
            <button id="logout">Logout</button>
        </div>
    </div>
    <div class="container2">
        @php
            $geo = (date('H') > 17) ? 'Malam' : 'Siang';
        @endphp
        Selamat {{ $geo }}
    </div>
    <div class="container2">
        @php
            $_email = explode('@', $email)[0];
            $_email = str_replace(['.', '_'], ' ', $_email)
        @endphp
        {{ ucwords($_email) }}
    </div>
    <div class="container2">
        Sekarang pukul {{ date('H:m:i') }}
    </div>
</div>

<script type="text/javascript" src="{{ asset('vendor/jquery/3.4.1/jquery.js') }}"></script>
<script>
    const passData = (url) => {
        try {
            let form = document.createElement("form");
            form.setAttribute("method", "get");
            form.setAttribute("action", url);

            document.body.appendChild(form)
            form.submit()
            form.remove()
        } catch (e) {
            console.error('Error in pass : ' + e);
        }
    }

    $('#logout').on('click', () => {
        passData('{{ route('login') }}')
    })
</script>

</body>
</html>
