<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div>
    <div>
        <h2>Halaman Login</h2>
    </div>
    <div class="alert">
        <div class="message">

        </div>
    </div>
    <div>
        <div>
            <label for="email">Email</label>
            <input type="email" id="email">
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" id="password">
        </div>
        <div>
            <button type="button" id="submit">
                Submit
            </button>
        </div>
    </div>

</div>

<script type="text/javascript" src="{{ asset('vendor/jquery/3.4.1/jquery.js') }}"></script>
<script>
    const passData = (url, variables) => {
        try {
            let form = document.createElement("form");
            form.setAttribute("method", "get");
            form.setAttribute("action", url);

            for (variable in variables) {
                const hiddenField = document.createElement("input");
                hiddenField.setAttribute("name", variable);
                hiddenField.setAttribute("value", variables[variable]);
                form.appendChild(hiddenField);
            }

            document.body.appendChild(form)
            form.submit()
            form.remove()
        } catch (e) {
            console.error('Error in pass : ' + e);
        }
    }

    $('#submit').on('click', () => {
        const email = $('#email').val()
        const password = $('#password').val()

        let valid = true

        const email_address_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        const pwd_regex = /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X]).*$/;

        if (!email || !email_address_regex.test(email)) {
            $('.alert .message').text('email tidak valid')
            console.log('email tidak valid')
            valid = false
        }
        if (!password || !pwd_regex.test(password)) {
            console.log('password tidak valid')
            valid = false
        }

        if (valid) {
            passData('{{ route('home') }}', {email})
        }
    })
</script>

</body>
</html>
